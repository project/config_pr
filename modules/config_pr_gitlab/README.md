Config Pull Request GitLab
==========================

This is a sub-module for Config Pull Request.
It adds support for GitLab. 

Dependencies
============
- m4tthumphrey/php-gitlab-api:^11
- guzzlehttp/guzzle:^7.8
- http-interop/http-factory-guzzle:^1.2

Installation
============
Use composer to make sure you will have all dependencies.
`composer require m4tthumphrey/php-gitlab-api:^11 guzzlehttp/guzzle:^7.8 http-interop/http-factory-guzzle:^1.2`
