Config Pull Request Github
==========================

This is a sub-module for Config Pull Request.
It adds support for Github. 

Dependencies
============
- php-http/guzzle7-adapter:^1.0
- knplabs/github-api:^3.16

Installation
============
Use composer to make sure you will have all dependencies.

`composer require php-http/guzzle7-adapter:^1.0 knplabs/github-api:^3.16`

