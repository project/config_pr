Config Pull Request BitBucket
=============================

This is a sub-module for Config Pull Request.
It adds support for BitBucket. 

Installation
============
Use composer to make sure you will have all dependencies.
`composer require bitbucket/client:^4.6`

Dependencies
============
- bitbucket/client:^4.6

