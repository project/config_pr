CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Config Pull Request module allows Admin users to request a pull request of
config changes on live environments. When last minute/urgent changes need to be
done on the Admin UI, the user can issue a Pull request that can be reviewed and
merged by the dev team.

Pros:

 * Speeds up the process of exporting last minute/urgent changes
 * Allows Admin users to tweak the configurations quickly and keep the changes
 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/config_pr
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/config_pr


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

Dev dependencies:
- Bitbucket client: bitbucket/client
- PHP GitHub API: knplabs/github-api
- GitLab API client for PHP: m4tthumphrey/php-gitlab-api
- Guzzle HTTP Adapter: php-http/guzzle7-adapter

INSTALLATION
------------

 * Install the Config Pull Request module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

Use composer to make sure you will have all dependencies.
`composer require drupal/config_pr:^1.0`

You need to enable the module for the repo client that you require,
these are the available options:
- Config Pull Request Github Support
- Config Pull Request Gitlab Support
- Config Pull Request BitBucket Support (Work in progress)

then use composer require for the dev libraries that you need i.e. For Gitlab
`composer require m4tthumphrey/php-gitlab-api:^11.14 guzzlehttp/guzzle:^7.8 http-interop/http-factory-guzzle:^1.2`

then use composer require for the dev libraries that you need i.e. For Github
`composer require m4tthumphrey/php-gitlab-api:^11.14 guzzlehttp/guzzle:^7.8 http-interop/http-factory-guzzle:^1.2`

CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to the /user page and add the Authentication token. To learn how
   to create authentication tokens, visit
   https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/
   and https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#creating-a-fine-grained-personal-access-token
   For Gitlab https://gitlab.com/-/user_settings/personal_access_tokens
3. Make sure you set the right permissions to allow the token to be used to create new branches and pull requests.
4. Navigate to the module settings page,
   /admin/config/development/configuration/pull_request/settings and add the
   repo user name and repo name. Normally these are found on the repo Url.
   i.e. https://github.com/marcelovani/captcha_keypad
   the username = marcelovani and repo name = captcha_keypad

How it works:

1. After the Admin is done with the changes, they will visit the
   Configuration Management page, select the Pull Request tab, select the
   configs that they want to keep in the Pull Request. They will confirm the
   repo url and give it a title and description, then select the submit
   button.
1. The module will check user authentication on the repo and create the pull
   request. It can also notify devs about the Pull Request.
1. The devs will review, comment, accept or reject the Pull Request.

Creating pull requests:

1. Visit the Config Manager page /admin/config/development/configuration and
   select the 'Pull Request' tab.
1. Select the configs you want to add to the pull request.
1. Fill in the pull request details and select the button.
1. After the form is submitted, you will see a message with the link to the
   pull request.
1. At the bottom of the page the user will see a list of open pull requests
   for the relevant repo.

**IMPORTANT**
You can create pull requests only after the initial config has been pushed to the repository.

MAINTAINERS
-----------

 * Marcelo Vani (marcelovani) - https://www.drupal.org/u/marcelovani

Supporting organization:

 * Dennis - https://www.drupal.org/dennis
